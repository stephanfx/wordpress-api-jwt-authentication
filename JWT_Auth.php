<?php
/**
 * Plugin Name: JWT_Auth
 * Description: Basic Authentication handler for the JSON API, used for development and debugging purposes
 * Author: WordPress API Team
 * Author URI: https://github.com/WP-API
 * Version: 0.1
 * Plugin URI: https://github.com/WP-API/Basic-Auth
 */

require_once plugin_dir_path( __FILE__ ) . 'Authentication/JWT.php';
require_once plugin_dir_path( __FILE__ ) . 'Exceptions/BeforeValidException.php';
require_once plugin_dir_path( __FILE__ ) . 'Exceptions/ExpiredException.php';
require_once plugin_dir_path( __FILE__ ) . 'Exceptions/SignatureInvalidException.php';

/**
* JWT Auth Class
*/
class JWT_Auth
{
	protected $key = "SomeGUIDToGoHere";
	protected $json_auth_error;

	public function register_routes($routes)
	{

		$routes['/jwt/authenticate'] = array(
			array( array($this, 'authenticate'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON),
		);
		return $routes;
	}

	public function authenticate($data)
	{
		$username = isset($data['username']) ? $data['username']: null;
		$password = isset($data['password']) ? $data['password']: null;
		if (!$username OR !$password) {
			return new WP_Error('json_bad_request', 'Missing parameters', array('status' => 400));
		}

		$user = wp_authenticate( $data['username'], $data['password'] );

		if ( is_wp_error( $user ) ) {
			$this->json_auth_error = $user;
			return $this->json_auth_error;
		}

		$key = $this->key;
		$token = array(
		    "iss" => $_SERVER['SERVER_ADDR'],
		    "aud" => $_SERVER['REMOTE_ADDR'],
		    "iat" => time(),
		    "nbf" => 1357000000,
		    "user" => $user->data,
		);
		$encoded = JWT::encode($token, $this->key);

		return array('token' => $encoded);
	}

	public function check_token($user)
	{
		// Don't authenticate twice
		if ( ! empty( $user ) ) {
			// return $user;
		}

		$headers = getallheaders();
		$token = "null";
		foreach ($headers as $key => $value) {
			if ('token' == strtolower($key)){
				$token = $value;
			}
		}

		if ($token == "null") {
			return $user;
		}

		//@todo: more checking if the Token is valid
		// for now, just decoding successfully is enough.
		try {
			$decoded = JWT::decode($token, $this->key);

			$user_id = $decoded->user->ID;
			$user = get_user_by( 'id', $user_id );

			$wp_json_basic_auth_error = true;
			$this->json_auth_error = true;
			if (!$user){
				return false;
			} else {
				return $user->ID;
			}
		} catch (UnexpectedValueException $e){
			$this->json_auth_error = $e;
			return null;
		}
	}

	public function auth_error($error)
	{
		// Passthrough other errors
		if ( ! empty( $error ) ) {
			return $error;
		}

		return $this->json_auth_error;

	}
}

function jwt_api_init() {
	global $jwt_auth;

	$jwt_auth = new JWT_Auth();
	add_filter( 'json_endpoints', array( $jwt_auth, 'register_routes' ));
}
add_action( 'wp_json_server_before_serve', 'jwt_api_init' );

//this will cause crap... but hey, it works?
$jwt_auth = new JWT_Auth();
add_filter( 'determine_current_user', array($jwt_auth, 'check_token'), 20 );
add_filter( 'json_authentication_errors', array($jwt_auth, 'auth_error') );


if (!function_exists('getallheaders'))  {
    function getallheaders()
    {
        if (!is_array($_SERVER)) {
            return array();
        }

        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}
